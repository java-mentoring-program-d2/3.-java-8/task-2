@FunctionalInterface
public interface MyFunctionalInterface {
    String intsToString(int a, int b);

    default int sum(int a, int b) {return a + b;}
    default int multiple(int a, int b) {return a + b;}

    static int minus(int a, int b) {return a - b;}
}
