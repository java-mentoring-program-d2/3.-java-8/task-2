import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Test {
    Supplier<String> supplier = () -> "Hello World!";
    Consumer<String> consumer = System.out::println;
    Predicate<String> predicate = s -> s.length() > 7;
    Function<String, Integer> function = String::length;

    MyFunctionalInterface myFunctionalInterface = (a, b) -> String.valueOf(a + b) + MyFunctionalInterface.minus(a, b);
    MyFunctionalInterface myFunctionalInterface2 = new MyFunctionalInterface() {
        @Override
        public String intsToString(int a, int b) {
            int c = MyFunctionalInterface.minus(a, b);
            return String.valueOf(multiple(a, c) + sum(a, b));
        }
    };

    public static void main(String[] args) {

    }
}
